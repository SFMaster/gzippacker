﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace GZipPacker
{
    class ProcessingOfDataBlocksThreads
    {
        //For data transfer
        private static long _globalId = 0;
        public static Queue<DataBlock> queueReadDataBlocks = new Queue<DataBlock>();
        public static SortedDictionary<long, byte[]> listDataBuffersForWrite = new SortedDictionary<long, byte[]>();

        //For threads
        private static short _numberOfThreads;
        public static Thread[] threads;
        public static bool ProcessingOff = false;   //stop flag
        private IReadFile _readFile;

        //For synchronization threads
        public static Object locker = new Object();

        IProcessing processing;

        //constructor
        public ProcessingOfDataBlocksThreads(IProcessing processing, short numberOfThreads, IReadFile ReadFile)
        {
            _numberOfThreads = numberOfThreads;
            this.processing = processing;
            _readFile = ReadFile;

            threads = new Thread[_numberOfThreads];
        }

        public void RunProcessingOfDataBlocksThreads(IProcessing processing)
        {
            try
            {
                for (int i = 0; i < _numberOfThreads; i++)
                {
                    threads[i] = new Thread(ThreadProcessingOfDataBlocks);
                    threads[i].Name = "ThreadProcessingOfDataBlocks " + i;
                    threads[i].Start();
                }
            }
            catch (ArgumentNullException exception)
            {
                GZipPacker.ShowErrorMessage("Error creating processing threads! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }

        }

        //processing method thread
        private void ThreadProcessingOfDataBlocks()
        {
            try
            {
                while (!ProcessingOff)
                {
                    long id = -1;
                    DataBlock dataBlock = null;
                    lock (locker)
                    {
                        var queueCount = queueReadDataBlocks.Count;
                        if (queueCount > 20)
                        {
                            _readFile.autoReset.WaitOne();
                        }
                        else
                        {
                            _readFile.autoReset.Set();
                        }
                        if (queueCount > 0 && !ProcessingOff)
                        {
                            dataBlock = queueReadDataBlocks.Dequeue();
                            id = Interlocked.Read(ref _globalId);
                            Interlocked.Increment(ref _globalId);
                        }
                        else if (!_readFile.readOFF) //if reads is working
                        {
                            continue;
                        }
                        else
                        {
                            ProcessingOff = true;   //compress threads stop working
                            break;
                        }
                    }


                    DataBlock dataBlockForWrite = processing.ProcessDataBlock(id, dataBlock);
                    lock (locker)
                    {
                        //Adds in SortedList for write in the file
                        listDataBuffersForWrite.Add(id, dataBlockForWrite.Buffer);
                    }                    
                }
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Error in threads processing of data blocks! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
                return;
            }
        }
    }
}
