﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipPacker
{
    class ReadFileForDecompress : IReadFile
    {
        private readonly string _originalFileName;
        private int _dataBufferSize;
        //Read thread
        public static Thread threadReadFile;

        public static bool readOFF = false;
        bool IReadFile.readOFF
        {
            get { return readOFF; }
            set { readOFF = value; }
        }

        public static AutoResetEvent autoReset = new AutoResetEvent(true);
        AutoResetEvent IReadFile.autoReset { get => autoReset; set => autoReset = value; }

                
        public ReadFileForDecompress(string originalFileName, int DataBufferSize)
        {
            _originalFileName = originalFileName;
            _dataBufferSize = DataBufferSize;
        }

        public void RunReadFileThread()
        {
            try
            {
                threadReadFile = new Thread(ReadFileProcess);
                threadReadFile.Name = "ReadFileThreadDecompress";
                threadReadFile.Start(_dataBufferSize);
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Thread the file read is not create! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }
        }

        public void ReadFileProcess(object param)
        {
            try
            {
                using (FileStream fsSource = new FileStream(_originalFileName, FileMode.Open, FileAccess.Read))
                {
                    while (!readOFF)
                    {
                        int countBytes = 0;   //количество прочитанных байтов

                        byte[] fieldId = new byte[8];
                        long id = -1;
                        countBytes = fsSource.Read(fieldId, 0, 8);
                        if (countBytes > 0)
                        {
                            id = BitConverter.ToInt64(fieldId, 0);
                        }

                        byte[] fieldNumberOfCompressBytes = new byte[4];
                        int NumberOfCompressBytes = 0;
                        countBytes = fsSource.Read(fieldNumberOfCompressBytes, 0, 4);
                        if (countBytes > 0)
                        {
                            NumberOfCompressBytes = BitConverter.ToInt32(fieldNumberOfCompressBytes, 0);
                        }

                        byte[] fieldNumberOfDecompressBytes = new byte[4];
                        int NumberOfDecompressBytes = 0;
                        countBytes = fsSource.Read(fieldNumberOfDecompressBytes, 0, 4);
                        if (countBytes > 0)
                        {
                            NumberOfDecompressBytes = BitConverter.ToInt32(fieldNumberOfDecompressBytes, 0);
                        }

                        byte[] compressedBuffer = new byte[NumberOfCompressBytes];
                        DataBlock dataBlock = null;
                        countBytes = fsSource.Read(compressedBuffer, 0, NumberOfCompressBytes);
                        if (countBytes > 0)
                        {
                            dataBlock = new DataBlock(id, NumberOfCompressBytes, NumberOfDecompressBytes, compressedBuffer);
                        }

                        if (countBytes > 0 && dataBlock != null)
                        {
                            autoReset.WaitOne();
                            lock (ProcessingOfDataBlocksThreads.locker)
                            {
                                ProcessingOfDataBlocksThreads.queueReadDataBlocks.Enqueue(dataBlock);
                            }
                        }
                        else
                        {
                            Console.WriteLine("Read file complete...");
                            readOFF = true;     //read the file thread will be turned off
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("File read error! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }
        }
    }
}
