﻿namespace GZipPacker
{
    internal class DataBlock
    {
        public long Id { get; }
        public int NumberOfCompressBytes { get; }
        public int NumberOfDecompressBytes { get; }
        public byte[] Buffer { get; }

        public DataBlock(byte[] Buffer)
        {
            this.Buffer = Buffer;
        }

        public DataBlock(long Id, int NumberOfCompressBytes, int NumberOfDecompressBytes, byte[] Buffer)
        {
            this.Id = Id;
            this.NumberOfCompressBytes = NumberOfCompressBytes;
            this.NumberOfDecompressBytes = NumberOfDecompressBytes;
            this.Buffer = Buffer;
        }
    }
}
