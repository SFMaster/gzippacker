﻿using System;
using System.IO;

namespace GZipPacker
{
    /*
     * Program to pack or unpack data from one file to another
     */
    public class Program
    {
        private static string originalFileName;
        private static string rezultFileName;
        private static WorkMode mode;

        public static void Main(string[] args)
        {
            //GZipTest.exe compress/decompress [originalFileName] [rezultFileName]

            if (args.Length == 3)
            {
                if (args[0].Equals("compress", StringComparison.InvariantCultureIgnoreCase))
                {
                    mode = WorkMode.Compress;
                    Console.WriteLine("Working mode: compress");
                }
                else if (args[0].Equals("decompress", StringComparison.InvariantCultureIgnoreCase))
                {
                    mode = WorkMode.Decompress;
                    Console.WriteLine("Working mode: decompress");
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Command entered incorrectly! Working mode: \"{0}\" not exist", args[0]);
                    return;
                }

                originalFileName = args[1];
                Console.WriteLine("Original file:\t {0}", originalFileName);

                rezultFileName = args[2];
                Console.WriteLine("Rezult file:\t {0}", rezultFileName);
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Command entered incorrectly!");
                return;
            }

            Run(mode, originalFileName, rezultFileName);

        }
                
        static void Run(WorkMode mode, string originalFileName, string rezultFileName)
        {
            if (!File.Exists(originalFileName))
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("The source file does not exist! Repeat the second parameter");
                return;
            }
            else
            {
                GZipPacker gZipPacker = new GZipPacker(mode, originalFileName, rezultFileName);
                gZipPacker.RunThreadsObjects();                
            }
        }
    }
}
