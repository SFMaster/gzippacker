﻿using System;
using System.Threading;

namespace GZipPacker
{
    internal interface IReadFile
    {
        bool readOFF { get; set; }
        AutoResetEvent autoReset { get; set; }
        void RunReadFileThread();
        void ReadFileProcess(Object param);
    }
}