﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipPacker
{
    class ReadFileForCompress : IReadFile
    {
        private readonly string _originalFileName;
        private int _dataBufferSize;

        //Read thread
        public static Thread threadReadFile;
        public static bool readOFF = false;
        bool IReadFile.readOFF
        {
            get { return readOFF; }
            set { readOFF = value; }
        }

        public static AutoResetEvent autoReset = new AutoResetEvent(true);
        AutoResetEvent IReadFile.autoReset { get => autoReset; set => autoReset = value; }
        
        public ReadFileForCompress(string originalFileName, int DataBufferSize)
        {   
            _originalFileName = originalFileName;
            _dataBufferSize = DataBufferSize;
        }

        public void RunReadFileThread()
        {
            try
            {
                threadReadFile = new Thread(ReadFileProcess);
                threadReadFile.Name = "ReadFileThreadCompress";
                threadReadFile.Start(_dataBufferSize);
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Thread the file read is not create! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }
        }

        public void ReadFileProcess(object param)
        {
            int bufferSizeForRead = (int)param;
            try
            {
                using (FileStream fsSource = new FileStream(_originalFileName, FileMode.Open, FileAccess.Read))
                {
                    while (!readOFF)
                    {
                        byte[] bufferForReadBytes = new byte[bufferSizeForRead];
                        int numberOfBytesRead = fsSource.Read(bufferForReadBytes, 0, bufferSizeForRead);
                        if (numberOfBytesRead > 0)
                        {
                            if (numberOfBytesRead < bufferSizeForRead)
                            {
                                byte[] buffer = new byte[numberOfBytesRead];
                                for (int i = 0; i < numberOfBytesRead; i++)
                                {
                                    buffer[i] = bufferForReadBytes[i];
                                }
                                autoReset.WaitOne();
                                lock (ProcessingOfDataBlocksThreads.locker)
                                {
                                    ProcessingOfDataBlocksThreads.queueReadDataBlocks.Enqueue(new DataBlock(buffer));
                                }
                            }
                            else
                            {
                                autoReset.WaitOne();
                                lock (ProcessingOfDataBlocksThreads.locker)
                                {
                                    ProcessingOfDataBlocksThreads.queueReadDataBlocks.Enqueue(new DataBlock(bufferForReadBytes));
                                }
                            }
                        }
                        else
                        {
                            Console.WriteLine("Read file complete...");
                            readOFF = true;     //read the file thread will be turned off
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("File read error! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }
        }
    }
}
