﻿using System;
using System.IO;
using System.IO.Compression;

namespace GZipPacker
{
    internal class GZipDecompressor : IProcessing
    {
        public DataBlock ProcessDataBlock(long id, DataBlock dataBlock)
        {
            DataBlock dataBlockForWrite = Decompress(dataBlock);
            return dataBlockForWrite;
        }

        public DataBlock Decompress(DataBlock dataBlock)
        {
            byte[] buffer = new byte[dataBlock.NumberOfDecompressBytes];
            try
            {
                using (MemoryStream ms = new MemoryStream(dataBlock.Buffer))
                {
                    using (GZipStream decompressionStream = new GZipStream(ms, CompressionMode.Decompress, true))
                    {
                        int n = decompressionStream.Read(buffer, 0, dataBlock.NumberOfDecompressBytes);
                    }
                }
                return new DataBlock(buffer);
            }
            catch (Exception exception)
            {                
                GZipPacker.ShowErrorMessage("Error decompressing buffer! Error message:" + exception.Message);
                throw;
            }
        }
    }
}
