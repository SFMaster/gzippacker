﻿namespace GZipPacker
{
    interface IProcessing
    {
        DataBlock ProcessDataBlock(long id, DataBlock dataBlock);                       
    }
}
