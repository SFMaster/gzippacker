﻿using System;

namespace GZipPacker
{

    public enum WorkMode : int { Compress, Decompress };  //Works mode

    //GZipPacker -> Main class
    public class GZipPacker
    {
        private readonly WorkMode mode;
        private readonly string originalFileName;
        private readonly string rezultFileName;

        private static IReadFile _readFile;
        private static IProcessing _processing;
        
        private const short NUMBER_OF_THREADS = 10;  //Number of threads the compress and decompres algorihm
        private const int DATA_BUFFER_SIZE = 1048576;  //The size of the data buffer
        public static bool theProgramIsFinished = true;

        ProcessingOfDataBlocksThreads processingOfDataBlocksThreads;
        WriteFileThread writeFileThread;

        public GZipPacker(WorkMode mode, string originalFileName, string rezultFileName)
        {
            this.mode = mode;
            this.originalFileName = originalFileName;
            this.rezultFileName = rezultFileName;

            CreateObjects();
        }

        //reates all objects for process
        private void CreateObjects()
        {
            switch (mode)
            {
                case WorkMode.Compress:
                    _readFile = new ReadFileForCompress(originalFileName, DATA_BUFFER_SIZE);
                    _processing = new GZipCompressor();
                    break;
                case WorkMode.Decompress:
                    _readFile = new ReadFileForDecompress(originalFileName, DATA_BUFFER_SIZE);
                    _processing = new GZipDecompressor();
                    break;
            }
            processingOfDataBlocksThreads = new ProcessingOfDataBlocksThreads(_processing, NUMBER_OF_THREADS, _readFile);
            writeFileThread = new WriteFileThread(rezultFileName);              //Thread writing the file            
        }

        //runs all threads
        public void RunThreadsObjects()
        {
            _readFile?.RunReadFileThread();
            processingOfDataBlocksThreads?.RunProcessingOfDataBlocksThreads(_processing);
            writeFileThread?.RunWriteFile();

            if (_readFile == null || processingOfDataBlocksThreads == null || writeFileThread == null)
            {
                ShowErrorMessage("Error create program objects!");
                Environment.Exit(0);
            }
        }

        //stops all threads and Exit
        public static void StopAllProcess()
        {
            theProgramIsFinished = false;

            _readFile.readOFF = true;
            ProcessingOfDataBlocksThreads.ProcessingOff = true;
            WriteFileThread.writeOFF = true;

            ShowErrorMessage("Program terminated with errors");
            Console.ReadKey();
            Environment.Exit(0);
        }

        public static void ShowErrorMessage(string ErrorMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(ErrorMessage);
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
