﻿using System;
using System.IO;
using System.IO.Compression;

namespace GZipPacker
{
    internal class GZipCompressor : IProcessing
    {
        public DataBlock ProcessDataBlock(long id, DataBlock dataBlock)
        {
            byte[] compressBuffer = Compress(dataBlock);
            byte[] fieldId = BitConverter.GetBytes(id);
            byte[] fieldNumberOfCompressBytes = BitConverter.GetBytes(compressBuffer.Length);
            byte[] fieldNumberOfDecompressBytes = BitConverter.GetBytes(dataBlock.Buffer.Length);

            byte[] compressedBuffer = AssembleCompressedBuffer(fieldId, fieldNumberOfCompressBytes, fieldNumberOfDecompressBytes, compressBuffer);

            return new DataBlock(compressedBuffer);
        }

        private byte[] Compress(DataBlock dataBlock)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (GZipStream compressionStream = new GZipStream(ms, CompressionMode.Compress, true))
                    {
                        compressionStream.Write(dataBlock.Buffer, 0, dataBlock.Buffer.Length);
                    }                    
                    return ms.ToArray();
                }                
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Error compressing buffer! Error message:" + exception.Message);
                throw exception;
            }
        }

        //Creats the data buffer for write in the file -> returns the packed buffer for write in the file
        private byte[] AssembleCompressedBuffer(byte[] fieldId, byte[] fieldNumberOfCompressBytes, byte[] fieldNumberOfDecompressBytes, byte[] compressBuffer)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(fieldId, 0, fieldId.Length);
                    ms.Write(fieldNumberOfCompressBytes, 0, fieldNumberOfCompressBytes.Length);
                    ms.Write(fieldNumberOfDecompressBytes, 0, fieldNumberOfDecompressBytes.Length);
                    ms.Write(compressBuffer, 0, compressBuffer.Length);
                    return ms.ToArray();
                }
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Error assemble compressing buffer!! Error message:" + exception.Message);
                throw exception;
            }

        }
    }
}
