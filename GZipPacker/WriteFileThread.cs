﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

namespace GZipPacker
{
    internal sealed class WriteFileThread
    {
        private readonly string _rezultFileName;

        //Write thread
        public static Thread threadWriteFile;

        //For get status the threads compressing or decompressing
        private static Thread[] _threads;
        public static bool writeOFF = false;
        
        private SortedDictionary<long, byte[]> _listForWrite;

        //constructor
        public WriteFileThread(string rezultFileName)
        {            
            _rezultFileName = rezultFileName;

            _threads = ProcessingOfDataBlocksThreads.threads;

            _listForWrite = ProcessingOfDataBlocksThreads.listDataBuffersForWrite;                        
        }

        public void RunWriteFile()
        {
            Console.WriteLine("The recording process is started! Wait for the process to finish...");
            try
            {
                threadWriteFile = new Thread(ThreadWriteFile);
                threadWriteFile.Name = "ThreadWriteFile";
                threadWriteFile.Start();
                threadWriteFile.Join();
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("Thread the file write is not create! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }

            //program finished
            if (GZipPacker.theProgramIsFinished)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("The file is completed...");
                Console.WriteLine("The program was completed !");
            }
            else
            {
                GZipPacker.ShowErrorMessage("The file is not completed !!!");
            }
        }

        //method of writing to a file(thread method)
        private void ThreadWriteFile()
        {
            try
            {
                using (FileStream rezultFileStream = new FileStream(_rezultFileName, FileMode.Create, FileAccess.Write))
                {
                    long id = 0;
                    while (!writeOFF)
                    {
                        if (_listForWrite.Count > 0)
                        {
                            lock (ProcessingOfDataBlocksThreads.locker)
                            {

                                byte[] buffer;
                                var pair = _listForWrite.First();
                                if (id == pair.Key)
                                {
                                    id++;
                                    buffer = pair.Value;
                                    _listForWrite.Remove(pair.Key);
                                    rezultFileStream.Write(buffer, 0, buffer.Length);
                                }
                                else
                                {
                                    continue;
                                }
                            }
                        }
                        else if (!GetThreadStatus())
                        {
                            continue;
                        }
                        else
                        {
                            writeOFF = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                GZipPacker.ShowErrorMessage("File read error! Error message:" + exception.Message);
                GZipPacker.StopAllProcess();
            }            
        }

        //Gets status the compress or decompress threads.
        //If compress or decompress threads is OFF returns true
        public static bool GetThreadStatus()
        {
            int counterThreadsOff = 0;
            foreach (var thread in _threads)
            {
                if (!thread.IsAlive)
                {
                    counterThreadsOff++;
                }
            }
            if (counterThreadsOff == _threads.Length)
                return true;
            else
                return false;
        }
    }
}
