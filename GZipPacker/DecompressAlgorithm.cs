﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipPacker
{

    internal sealed class DecompressAlgorithmThread
    {
        //For data transfer
        public static Queue<DataBlock> queueForDecompressDataBlocks = new Queue<DataBlock>();
        public static SortedDictionary<long, byte[]> ListDecompressedDataBuffersForWrite = new SortedDictionary<long, byte[]>();

        //For decompress threads
        private static short _numberOfThreads;
        public static Thread[] threads;
        public static bool decompressedOff = false;

        //For synchronization threads
        public static Object locker = new Object();

        //constructor
        public DecompressAlgorithmThread(short numberOfThreads)
        {
            _numberOfThreads = numberOfThreads;

            threads = new Thread[_numberOfThreads];

        }

        public void RunDecompressAlgorithmThreads()
        {
            for (int i = 0; i < _numberOfThreads; i++)
            {
                threads[i] = new Thread(ThreadDecompressAlgorithm);
                threads[i].Name = "ThreadDecompressAlgorithm " + i;
                threads[i].Start();
            }
        }

        //decompress method thread
        private void ThreadDecompressAlgorithm()
        {
            while (!decompressedOff)
            {
                int countQeue;
                DataBlock compressDataBlock;
                lock (locker)
                {
                    countQeue = queueForDecompressDataBlocks.Count;
                    if (countQeue > 0 && !decompressedOff)
                    {
                        compressDataBlock = queueForDecompressDataBlocks.Dequeue();                        
                    }
                    else if (!ReadFileThread.readOFF)   //if reads is working
                    {                        
                        continue;
                    }
                    else
                    {
                        decompressedOff = true;     //decompress threads stop working
                        break;
                    }
                }
                
                byte[] decompressedBufferForWrite = DecompressBuffer(compressDataBlock);

                lock (locker)
                {
                    //Adds in SortedList for write in the file
                    ListDecompressedDataBuffersForWrite.Add(compressDataBlock.Id, decompressedBufferForWrite);
                }                
            }
        }

        //Unpacks the data buffer -> Returns the unpacked buffer
        private byte[] DecompressBuffer(DataBlock dataBlock)
        {
            byte[] buffer = new byte[dataBlock.NumberOfDecompressBytes];
            try
            {
                using (MemoryStream ms = new MemoryStream(dataBlock.Buffer))
                {
                    using (GZipStream decompressionStream = new GZipStream(ms, CompressionMode.Decompress, true))
                    {
                        int n = decompressionStream.Read(buffer, 0, dataBlock.NumberOfDecompressBytes);
                    }
                }
                return buffer;
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error decompressing buffer! Error message:" + exception.Message);
                return null;
            }
            
        }       
    }
}
