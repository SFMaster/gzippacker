﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GZipPacker
{
    internal sealed class CompressAlgorithmThread
    {
        //For data transfer
        private static long _globalId = 0;
        public static Queue<byte[]> queueForCompressDataBuffers = new Queue<byte[]>();
        public static SortedDictionary<long, byte[]> ListCompressedDataBuffersForWrite = new SortedDictionary<long, byte[]>();
                
        //For compress threads
        private static short _numberOfThreads;
        public static Thread[] threads;        
        public static bool compressedOff = false;

        //For synchronization threads
        public static Object locker = new Object();
        
        //constructor
        public CompressAlgorithmThread(short numberOfThreads)
        {
            _numberOfThreads = numberOfThreads;

            threads = new Thread[_numberOfThreads];
                                   
        }

        public void RunCompressAlgorithmThreads()
        {
            for (int i = 0; i < _numberOfThreads; i++)
            {
                threads[i] = new Thread(ThreadCompressAlgorithm);
                threads[i].Name = "ThreadCompressAlgorithm " + i;
                threads[i].Start();
            }
        }

        //compress method thread
        private void ThreadCompressAlgorithm()
        {
            while (!compressedOff)
            {
                int countQeue;
                byte[] decompressBuffer;
                long id;
                lock (locker)
                {
                    countQeue = queueForCompressDataBuffers.Count;
                    if (countQeue > 0 && !compressedOff)
                    {
                        decompressBuffer = queueForCompressDataBuffers.Dequeue();
                        id = Interlocked.Read(ref _globalId);
                        Interlocked.Increment(ref _globalId);                        
                    }
                    else if (!ReadFileThread.readOFF) //if reads is working
                    {
                        continue;
                    }
                    else
                    {
                        compressedOff = true;   //compress threads stop working
                        break;
                    }
                }
                
                byte[] compressBuffer = CompressBuffer(decompressBuffer);                
                byte[] fieldId = BitConverter.GetBytes(id);                
                byte[] fieldNumberOfCompressBytes = BitConverter.GetBytes(compressBuffer.Length);
                byte[] fieldNumberOfDecompressBytes = BitConverter.GetBytes(decompressBuffer.Length);

                byte[] compressedBuffer = AssembleCompressedBuffer(fieldId, fieldNumberOfCompressBytes, fieldNumberOfDecompressBytes, compressBuffer);

                lock (locker)
                {
                    //Adds in SortedList for write in the file
                    ListCompressedDataBuffersForWrite.Add(id ,compressedBuffer);
                }                
            }
        }

        //Creats the data buffer for write in the file -> returns the packed buffer for write in the file
        private byte[] AssembleCompressedBuffer(byte[] fieldId, byte[] fieldNumberOfCompressBytes, byte[] fieldNumberOfDecompressBytes, byte[] compressBuffer)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(fieldId, 0, fieldId.Length);
                    ms.Write(fieldNumberOfCompressBytes, 0, fieldNumberOfCompressBytes.Length);
                    ms.Write(fieldNumberOfDecompressBytes, 0, fieldNumberOfDecompressBytes.Length);
                    ms.Write(compressBuffer, 0, compressBuffer.Length);
                    return ms.ToArray();
                }
            }
            catch(Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error assemble compressing buffer!! Error message:" + exception.Message);
                return null;
            }
            
        }

        //Packs the data buffer -> Returns the packed buffer
        private byte[] CompressBuffer(byte[] buffer)
        {
            //сжимаем          
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    using (GZipStream compressionStream = new GZipStream(ms, CompressionMode.Compress, true))
                    {
                        compressionStream.Write(buffer, 0, buffer.Length);
                    }
                    return ms.ToArray();
                }
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error compressing buffer! Error message:" + exception.Message);
                return null;
            }
            
        }
    }
}
